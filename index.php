<?php

function sampleCards($amount){
    $output = '';

    for($i=0; $i<$amount; $i++){
        $output .= '<div class="card">
            <svg class="icon" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" ><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" ></path></svg>
            <span class="title">Title</span>
        </div>';
    }

    return $output;
}

function sampleArticles($amount){
    $output = '';

    for($i=0; $i<$amount; $i++){

        $randSize = mt_rand(400,2000);

        $output .= '<div class="two-col-blocks">
        <div class="block-img">
            <img src="https://picsum.photos/'.$randSize.'" alt="sample img"/>
        </div>
        <div class="block-text">
            <div class="block-content">
                <h3>Title</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a neque massa. Donec vitae ipsum porttitor
                    tellus vestibulum pulvinar a et arcu. Donec suscipit tempus risus, vitae volutpat ex rutrum a. Interdum et
                    malesuada fames ac ante ipsum primis in faucibus.</p>
            </div>

        </div>
    </div>';
    }

    return $output;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hero</title>

    <link href='https://fonts.googleapis.com/css?family=Khand' rel='stylesheet'>
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


</head>


<body>

<section class="section hero full-width">

    <h2>Webo antraste</h2>

    <a href="#lyrics" class="hero-button">linkas</a>

</section>

<section class="section lyrics" id="lyrics">
    <h1>Lyricsai</h1>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a neque massa. Donec vitae ipsum porttitor
        tellus vestibulum pulvinar a et arcu. Donec suscipit tempus risus, vitae volutpat ex rutrum a. Interdum et
        malesuada fames ac ante ipsum primis in faucibus. Fusce vulputate id ligula et aliquam. Maecenas urna orci,
        placerat vitae nulla a, laoreet varius nunc. Aenean hendrerit egestas posuere. Etiam eget orci urna. Etiam
        sed placerat nibh. Cras id dolor diam. Cras quis aliquet metus.</p>

    <p>Sed convallis eleifend laoreet. Curabitur volutpat est elit, nec gravida ex pharetra id. Duis blandit
        molestie mauris, et vestibulum lacus iaculis id. Suspendisse eu bibendum massa. Donec id posuere nunc, eget
        pulvinar nunc. Praesent euismod venenatis leo, ut efficitur nunc placerat id. Pellentesque habitant morbi
        tristique senectus et netus et malesuada fames ac turpis egestas.</p>

    <p>Sed convallis eleifend laoreet. Curabitur volutpat est elit, nec gravida ex pharetra id. Duis blandit
        molestie mauris, et vestibulum lacus iaculis id. Suspendisse eu bibendum massa. Donec id posuere nunc, eget
        pulvinar nunc. Praesent euismod venenatis leo, ut efficitur nunc placerat id. Pellentesque habitant morbi
        tristique senectus et netus et malesuada fames ac turpis egestas.</p>

    <p>Sed convallis eleifend laoreet. Curabitur volutpat est elit, nec gravida ex pharetra id. Duis blandit
        molestie mauris, et vestibulum lacus iaculis id. Suspendisse eu bibendum massa. Donec id posuere nunc, eget
        pulvinar nunc. Praesent euismod venenatis leo, ut efficitur nunc placerat id. Pellentesque habitant morbi
        tristique senectus et netus et malesuada fames ac turpis egestas.</p>

</section>

<section class="section features">
    <h1>Features</h1>


    <div class="cards-wrapper">
        <?= sampleCards(40) ?>
    </div>


</section>

<section class="section articles">
    <h1>Articles</h1>
    <?= sampleArticles(10)?>

</section>



<section class="section todolist">
    <h1>Todo List</h1>

    <div class="two-col-blocks">
        <div class="block-text content">
            <ul class="list checked red-all">
                <li>item</li>
                <li>item</li>
                <li class="blue">item</li>
                <li>item</li>
                <li>item</li>
            </ul>
        </div>
        <div class="block-text content">
            <ul class="list check-circle blue-all">
                <li>item</li>
                <li class="red">item</li>
                <li>item</li>
                <li>item</li>
                <li>item</li>
            </ul>
        </div>
    </div>

</section>

<section class="section advert">
    <h1>Advertisment</h1>
    <div class="one-col-block">

        <div class="image-wrapper decorated">
            <img src="https://picsum.photos/500" alt="sample img"/>
        </div>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <button class="buy-button">Buy Now</button>
    </div>

</section>


<div class="header fixed">
    <nav>
        <ul>
            <li>Item</li>
            <li>Item</li>
            <li>Item</li>
        </ul>
    </nav>
</div>

</body>
</html>